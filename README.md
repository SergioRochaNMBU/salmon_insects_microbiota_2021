# Modulation of Atlantic salmon (_Salmo salar_) gut microbiota composition and predicted metabolic capacity by feeding diets with processed black soldier fly (_Hermetia illucens_) larvae meals and fractions

Black soldier fly (_Hermetia illucens_) is a promising insect species to use as a novel ingredient in fish feeds. Black soldier fly larvae consists of three major fractions, namely protein, lipid, and exoskeleton. These fractions contains bioactive compounds that can modulate the gut microbiota in fish such as antimicrobial peptides, lauric acid, and chitin. However, it is not certain how, or which fractions of black solider fly would affect gut microbiota in fish. In the present study, black soldier fly larvae were processed into three different meals (full-fat, defatted and de-chitinized) and two fractions (oil and exoskeleton), and included in diets for Atlantic salmon (_Salmo salar_). Atlantic salmon pre-smolts were fed with these diets in comparison with a commercial-like control diet for eight weeks to investigate the effects of insect meals and fractions on the composition and predicted metabolic capacity of gut microbiota. The gut microbiota was profiled by 16S rRNA gene sequencing, and the predicted metabolic capacities of gut microbiota were determined using genome-scale metabolic models. The inclusion of insect meals and fractions decreased abundance of _Proteobacteria_ and increased abundance of _Firmicutes_ in salmon gut. The diets that contained insect chitin, i.e., insect meals or exoskeleton diets, increased abundance of chitinolytic bacteria including lactic acid bacteria and _Actinomyces_ in salmon gut, while full-fat group showed the highest abundances. The diets that contained insect lipids, i.e., insect meals and oil diets enriched _Bacillaceae_ in fish gut. The fish fed full-fat insect meal had a unique gut microbiota dominated by beneficial bacteria such as lactic acid bacteria and _Actinomyces_, and showed a predicted increase in mucin degradation compared to the fish fed other diets. The gut microbiota in fish fed defatted meal had a predicted increase in starch and sucrose metabolism compared to control fish. The defatted and de-chitinized meals and insect oil gave a predicted increase in fatty acid synthesis in gut microbiota. Overall, the present results showed that the insect meals and fractions can differently modulate the composition and predicted metabolic capacity of gut microbiota in Atlantic salmon pre-smolts. 

Content of files in this project

- Code -- all the scripts used for the analysis 
    - functions -- functions for automating tasks
        - 01_plot_frequency.R -- function to be used in the code '02_Pre-processing.Rmd'
        - 02_plot_prevalance.R -- function to be used in the code '02_Pre-processing.Rmd'
        - 03_make_taxa_barplot.R -- function to be used in the code '03_Taxonomic_analysis.Rmd'
    - 01_Sequence_denoising_dada2.Rmd -- processing of raw data using DADA2 to generate ASV table
    - 02_Pre_processing.Rmd -- creating phyloseq object and pre-processing to remove features
    - 03_Taxonomic_analysis.Rmd -- analysis and visualization of taxonomic composition, core ASVs and microbial overlap
    - 04_Alpha_diversity.Rmd -- visualization and statistical analysis of alpha-diversity
    - 05_Beta_diversity.Rmd -- visualization and statistical analysis of beta-diversity
    - 06_Metabolic_reaction_analysis.ipynb -- analysis of predictive metabolic reactions
- Data -- all the data, including raw, reference and intermediate data
    - Data_Metabolic_reaction_analysis -- all the data used in metabolic reaction analysis
    - Figures -- for identification of contaminants
        - 01_prevalance_contam.pdf
        - 02_DNA_concentration.pdf
        - 03_frequency_contam.pdf
        - 04_prevalence_contam_mock.pdf
    - 00_Metadata.csv -- sample metadata
    - 01_PaboSeqtab.nochim.rds -- ASV table after removal of chimeras
    - 02_LL.taxa.rds -- taxonomy table
    - 03_sample_contaminants.xlsx -- removed contaminants
    - 04_ps_noncontam.rds -- phyloseq object after removal of contaminants
    - 05_ps_nocontam_mock.rds -- phyloseq object for positive control
    - 06_contam_neg.rds -- phyloseq object for negative control
    - 07_mock_merged_percent.xlsx -- taxa abundances in positive control samples
    - 08_neg_merged_percent.xlsx -- taxa abundances in negative control samples
    - 09_phylum_digesta_name.xlsx -- phylum abundances in digesta samples
    - 10_phylum_feed_name.xlsx -- phylum abundances in feed samples
    - 11_phylum_water_name.xlsx -- phylum abundances in water samples
    - 12_genus_digesta_name.xlsx -- genus/lower taxonony abundances in digesta samples
    - 13_genus_feed_name.xlsx -- genus/lower taxonony abundances in feed samples
    - 14_genus_water_name.xlsx -- genus/lower taxonony abundances in water samples
- LICENSE.md
- README.md

